
const St = imports.gi.St;
const Main = imports.ui.main;
const Tweener = imports.ui.tweener;
const Clutter = imports.gi.Clutter;
const Lang = imports.lang;
const Gio = imports.gi.Gio;

// Ui 
const PanelMenu = imports.ui.panelMenu;

// i18n
const Gettext = imports.gettext.domain('gnome-shell-extension-mobile-vikings');
const _ = Gettext.gettext;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Convenience = Me.imports.convenience;

const MobileVikingsIndicator = new Lang.Class({
    Name: 'MobileVikingsIndicator',
    Extends: PanelMenu.Button,

    _init: function () {
        this.parent(0.0, _("Mobile Vikings"));

        // Create icon
        let hbox = new St.BoxLayout({ style_class: 'panel-status-menu-box' });
        let icon = new St.Icon({ icon_name: 'mobile-vikings-indicator',
                                style_class: 'system-status-icon' });
        let label = new St.Label({ text: _("Mobile Vikings"),
                                   y_expand: true,
                                   y_align: Clutter.ActorAlign.CENTER });

        hbox.add_child(icon);
        hbox.add_child(label);
        this.actor.add_actor(hbox);
    },
    destroy: function () {
        this.parent();
    }
});

function init () {
    Convenience.initTranslations();
}

let _indicator;

function enable() {
    _indicator = new MobileVikingsIndicator;
    Main.panel.addToStatusArea('mobile-vikings-indicator', _indicator);
    // Main.panel._rightBox.insert_child_at_index(_indicator, 0);
}

function disable() {
    _indicator.destroy();
}
